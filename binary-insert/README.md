Bash Binary Insert
==================

Bash Binary Insert is a library of functions written in Bash to insert an item
into an array by performing a binary search to determine the insertion point.

Installation
============

Source `binary-insert` in any files where you need this functionality.

```Shell
# Inside of a source file
source /path/to/binary-insert
```

Usage
=====

binary\_insert\_num\_desc
-------------------------

```Shell
binary_insert_num_desc <array_name> <item>
```

`binary_insert_num_desc` expects the array *named by* `array-name` to be
sorted in descending numerical order (items with higher values at lower
indices). `item` may encode multiple pieces of information, but the first
word of `item` must be an integer, and the first word of each item acts as
the sort key. `item` will be inserted into the array in the correct postion.

**Note:** This function uses name references, so if you use it, your code
can't contain a variable named `_binary_insert_array`.

```Shell
declare -a sorted_array=('5 foo' '3 bar' '2 qux')
binary_insert_num_desc sorted_array '4 inserted')
# sorted_array=('5 foo' '4 inserted' '3 bar' '2 qux')
```

In the case of duplicate sort keys, `item` will be inserted before an item
already in the array with the same sort key.

```Shell
declare -a sorted_array=('5 foo' '3 bar' '2 qux')
binary_insert_num_desc sorted_array '2 inserted')
# sorted_array=('5 foo' '3 bar' '2 inserted' '2 qux')
```

