Bash Utilities
==============

A collection of utilites written in bash.

Best
----

A set of utility functions for unit testing shell scripts.

Binary Insert
-------------

Take a sorted array and an item to insert and perform a binary search to
find the insertion point, then insert the item.

